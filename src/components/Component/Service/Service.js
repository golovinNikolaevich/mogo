import React from 'react'
import './Service.scss'

export default (props) => {
  return(
    <div className="services__item">
      <h1 className="services__title">
        <img className="services__img" src={props.img} alt=""/>
        {props.title}
      </h1>
      <p className="services__text">{props.text}</p>
    </div>
  )
}