import React from 'react'
import './WhatWeDo.scss'
import ArticleTitle from '../../Component/ArticleTitle/ArticleTitle'
import Paragraph from '../../Component/Paragraph/Paragraph'
import CarouselItem from '../../Component/CarouselItem/CarouselItem'
// Images
import picture from '../../../img/picture.png'
import equalizer from '../../../img/equalizer.png'
import bullseye from '../../../img/bullseye.png'

class WhatWeDo extends React.Component {

  constructor(props) {
    super(props)
    
    this.state = {
      articleTitle: {
        title: 'Service',
        subtitle: 'What we do',
        isArticle: true
      },
      carouselItem: [
        {
          title: 'Photography',
          picture: picture,
          text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
          title: 'Creativity',
          picture: equalizer,
          text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
          title: 'Web Design',
          picture: bullseye,
          text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        }
      ]
    }
  }

  openText = (event) => {
    const el = document.querySelector('.doing__detail-advantages').childNodes
    const elArrow = document.querySelectorAll('.doing__detail-title .open')
    const evTr = event.target
    const hide = 'doing__detail-hide'

    el.forEach((el) => {
      el.childNodes[1].classList.add('doing__detail-hide')
    })

    elArrow.forEach((el) => {
      el.classList.remove('open')
    })

    if (evTr.classList.contains('doing__detail-title')) {
      evTr.nextSibling.classList.remove(hide)
      evTr.lastChild.classList.add('open')
    } else if (evTr.classList.contains('doing__detail-img')) {
      evTr.parentNode.parentNode.nextSibling.classList.remove(hide)
      evTr.parentNode.parentNode.lastChild.classList.add('open')
    } else if (evTr.classList.contains('doing__detail-open')) {
      evTr.parentNode.parentNode.lastChild.classList.remove(hide)
      evTr.classList.add('open')
    } else if (event.target.classList.contains('doing__detail-innerTitle')) {
      evTr.parentNode.nextSibling.classList.remove(hide)
      evTr.parentNode.lastChild.classList.add('open')
    }
  }

  render() {

    const articleTitle = this.state.articleTitle
    const carouselItem = this.state.carouselItem

    return(
      <section className="doing"> 
        <div className="wrap">
          
          <ArticleTitle
            isArticle={articleTitle.isArticle}
            title={articleTitle.title}
            subtitle={articleTitle.subtitle} />

          <Paragraph
            title={'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'} />

          <div className="doing__detail">
            <div className="doing__detail-photo"></div>
            <div className="doing__detail-advantages">
              {carouselItem.map((carouselItem, index) => {
                let firstElement = ''
                if (index === 0) {
                  firstElement = 'open'
                }
                return (
                  <CarouselItem
                    isFirst={firstElement}
                    key={index}
                    openText={this.openText}
                    picture={carouselItem.picture}
                    title={carouselItem.title}
                    text={carouselItem.text} />
                )
              })}
            </div>
          </div>

        </div>
      </section>
    )
  }
}

export default WhatWeDo