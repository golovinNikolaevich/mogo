import React from 'react'
import ArticleTitle from '../../Component/ArticleTitle/ArticleTitle'
import Paragraph from '../../Component/Paragraph/Paragraph'
import PhotoOurTeam from '../../Component/PhotoOurTeam/PhotoOurTeam'
// Images
import phototeam1 from '../../../img/phototeam1.png'
import phototeam2 from '../../../img/phototeam2.png'
import phototeam3 from '../../../img/phototeam3.png'

class MeetOurTeam extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      articleTitle: {
        title: 'Who we are',
        subtitle: 'Meet our team',
        isArticle: true
      },
      paragraph: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
      photoOurTeam: [
        {
          photography: phototeam1,
          title: 'Super Team',
          id: 1
        },
        {
          photography: phototeam2,
          title: 'Wonderful Users',
          id: 2
        },
        {
          photography: phototeam3,
          title: 'Great Place',
          id: 3
        }
      ]
    }
  }

  render() {

    const articleTitle = this.state.articleTitle
    const photoOurTeam = this.state.photoOurTeam

    return(
      <section className="our-team">
        <div className="wrap">
          
          <ArticleTitle
            isArticle={articleTitle.isArticle}
            title={articleTitle.title}
            subtitle={articleTitle.subtitle}
          />

          <Paragraph
            title={'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'}
          />

          <div className="photo-story photo-team">
            {photoOurTeam.map((photoOurTeam) => {
              return (
                <PhotoOurTeam
                  img={photoOurTeam.photography}
                  title={photoOurTeam.title}
                  key={photoOurTeam.id}
                />
              )
            })}
            </div>

        </div>
      </section>
    )
  }
}

export default MeetOurTeam