import React from 'react'
import ArticleTitle from '../../Component/ArticleTitle/ArticleTitle'

class PeopleSay extends React.Component {

  constructor() {
    super()
    this.title = 'Happy clients'
    this.subtitle = 'What people say'
    this.isArticle = true
  }

  render() {
    return(
      <section className="comments">
        <div className="wrap">
          
          <ArticleTitle
            isArticle={this.isArticle}
            title={this.title}
            subtitle={this.subtitle}
          />

        </div>
      </section>
    )
  }
}

export default PeopleSay