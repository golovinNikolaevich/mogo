import React from 'react'
import './CarouselItem.scss'

export default (props) => {

  let firstEl = `doing__detail-text doing__detail-hide`
  let firstOpenArrow = `doing__detail-open`

  if (props.isFirst) {
    firstEl = 'doing__detail-text'
    firstOpenArrow = 'doing__detail-open open'
  }

  return (
    <div className="doing__detail-item">
      <h1 className="doing__detail-title" onClick={props.openText}>
        <span><img className="doing__detail-img" src={props.picture} alt=""/></span>
        <span className="doing__detail-innerTitle">{props.title}</span>
        <span className={firstOpenArrow}></span>
      </h1>
      <div className={firstEl}>{props.text}</div>
    </div>
  )
}