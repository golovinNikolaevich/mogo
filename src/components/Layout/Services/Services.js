import React from 'react'
import ArticleTitle from '../../Component/ArticleTitle/ArticleTitle'
import Service from '../../Component/Service/Service'
// Images
import alarm from '../../../img/alarm.png'
import linegraph from '../../../img/linegraph.png'
import computer from '../../../img/computer.png'
import book from '../../../img/book.png'
import home from '../../../img/home.png'
import image from '../../../img/image.png'

class Services extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      articleTitle: {
        title: 'We work with',
        subtitle: 'Amazing Services',
        isArticle: true
      },
      service: [
        { title: 'Photography', img: alarm, text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.', id: 1 },
        { title: 'Web Design', img: linegraph, text: 'Ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.', id: 2 },
        { title: 'Creativity', img: computer, text: 'Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.', id: 3 },
        { title: 'Seo', img: book, text: 'Ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.', id: 4 },
        { title: 'Css/Html', img: home, text: 'Lorem dolor sit amet, consectetur adipiscing elit, sed do tempor.', id: 5 },
        { title: 'Digital', img: image, text: 'Sit amet, consectetur adipiscing elit, sed do eiusmod tempor.', id: 6 }
      ]
    }
  }

  render() {

    const articleTitle = this.state.articleTitle
    const service = this.state.service

    return(
      <section className="services">
        <div className="wrap">
          
          <ArticleTitle
            isArticle={articleTitle.isArticle}
            title={articleTitle.title}
            subtitle={articleTitle.subtitle}
          />

          <div className="services__block">
            {service.map((service) => {
              return (
                <Service
                  title={service.title}
                  img={service.img}
                  text={service.text}
                  key={service.id}
                />
              )
            })}
          </div>

        </div>
      </section>
    )
  }
}

export default Services