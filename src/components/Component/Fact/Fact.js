import React from 'react'
import './Fact.scss'

export default (props) => {
  return(
    <div className="facts__item">
      <span className="facts__count">{props.count}</span>
      <span className="facts__what">{props.what}</span>
    </div>
  )
}