import React from 'react';
import Header from './components/Layout/Header/Header'
import AboutUs from './components/Layout/AboutUs/AboutUs'
import Facts from './components/Layout/Facts/Facts'
import Services from './components/Layout/Services/Services'
import UniqueDesign from './components/Layout/UniqueDesign/UniqueDesign'
import WhatWeDo from './components/Layout/WhatWeDo/WhatWeDo'
import CommentSlider from './components/Layout/CommentSlider/CommentSlider'
import MeetOurTeam from './components/Layout/MeetOurTeam/MeetOurTeam'
import SomeOurWork from './components/Layout/SomeOurWork/SomeOurWork'
import PeopleSay from './components/Layout/PeopleSay/PeopleSay'
import LatestBlog from './components/Layout/LatestBlog/LatestBlog'
import OpenMap from './components/Layout/OpenMap/OpenMap'
import Footer from './components/Layout/Footer/Footer'

console.log('Woohoo!')

function App() {
  return (
    <>
      <Header />
      <main>
        <AboutUs />
        <Facts />
        <Services />
        <UniqueDesign />
        <WhatWeDo />
        <CommentSlider />
        <MeetOurTeam />
        <SomeOurWork />
        <PeopleSay />
        <LatestBlog />
        <OpenMap />
      </main>
      <Footer />
    </>
  );
}

export default App;
