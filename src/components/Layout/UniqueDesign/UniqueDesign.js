import React from 'react'
import './UniqueDesign.scss'
import ArticleTitle from '../../Component/ArticleTitle/ArticleTitle'
// Images
import tablet from '../../../img/tablet.png'
import mobile from '../../../img/mobile.png'

class UniqueDesign extends React.Component {

  constructor() {
    super()
    this.title = 'For all devices'
    this.subtitle = 'Unique design'
    this.isArticle = true
  }

  render() {
    return(
      <section className="design">
        <ArticleTitle
          isArticle={this.isArticle}
          title={this.title}
          subtitle={this.subtitle} />

          <div className="devices">
            <div className="devices__item">
              <img src={tablet} alt=""/>
            </div>
            <div className="devices__item">
              <img src={mobile} alt=""/>
            </div>
          </div>
      </section>
    )
  }
}

export default UniqueDesign