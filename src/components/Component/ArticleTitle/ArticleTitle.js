import React from 'react'
import './ArticleTitle.scss'

// kaushan-dark

const ArticleTitle = (props) => {

  let addClass = 'header__text-title heading-kaushan white-line';
  let addClass2 = 'heading-kaushan--main';
  let addClass3 = 'heading-kaushan--sub';

  if (props.isArticle) {
    addClass = 'heading-kaushan kaushan-dark red-line';
    addClass2 += ' kaushan-dark--main';
    addClass3 += ' kaushan-dark--sub';
  }

  return(
    <>
      <h1 className={addClass}>
        <span className={addClass2}>{props.title}</span>
        <span className={addClass3}>{props.subtitle}</span>
      </h1>
    </>
  )
}

export default ArticleTitle