import React from 'react'
import './Paragraph.scss'

export default (props) => (
  <p className='paragraph'>{props.title}</p>
)