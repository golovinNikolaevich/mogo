import React from 'react'
import './Navigation.scss'

class Navigation extends React.Component {

  render() {
    return(
      <>
        <nav className="navigation">
          <a href="#about">About</a>
          <a href="#service">Service</a>
          <a href="#work">Work</a>
          <a href="#blog">Blog</a>
          <a href="#concat">Contact</a>
        </nav>
        <div className="basket"></div>
        <div className="search"></div>
      </>
    )
  }
}

export default Navigation