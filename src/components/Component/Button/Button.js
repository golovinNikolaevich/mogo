import React from 'react'
import './Button.scss'

export default (props) => (
  <button className="btn">{props.btnName}</button>
)