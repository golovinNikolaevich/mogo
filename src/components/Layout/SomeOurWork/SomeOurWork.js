import React from 'react'
import ArticleTitle from '../../Component/ArticleTitle/ArticleTitle'
import Paragraph from '../../Component/Paragraph/Paragraph'

class SomeOurWork extends React.Component {

  constructor(props) {
    super(props)
    
    this.articleTitle = {
      title: 'What we do',
      subtitle: 'Some of our work',
      isArticle: true
    }
  }

  render() {

    const articleTitle = this.articleTitle

    return(
      <section className="our-work">
      
        <ArticleTitle
          isArticle={articleTitle.isArticle}
          title={articleTitle.title}
          subtitle={articleTitle.subtitle}
        />

        <Paragraph
          title={'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'}
        />

      </section>
    )
  }
}

export default SomeOurWork