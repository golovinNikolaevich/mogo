import React from 'react'
import ArticleTitle from '../../Component/ArticleTitle/ArticleTitle'
import Paragraph from '../../Component/Paragraph/Paragraph'
import PhotoStory from '../../Component/PhotoStory/PhotoStory'
// Photo
import aboutOne from '../../../img/about_1.png'
import aboutTwo from '../../../img/about_2.png'
import aboutThree from '../../../img/about_3.png'
import users from '../../../img/users.png'

class AboutUs extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      articleTitle: {
        title: 'What we do',
        subtitle: 'Story about us',
        isArticle: true
      },
      paragraph: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
      photo: [
        {
          photography: aboutOne,
          icons: users,
          title: 'Super Team',
          id: 1
        },
        {
          photography: aboutTwo,
          icons: users,
          title: 'Wonderful Users',
          id: 2
        },
        {
          photography: aboutThree,
          icons: users,
          title: 'Great Place',
          id: 3
        }
      ]
    }
  }

  changeParagraph = (yeah) => {
    this.setState({
      paragraph: yeah
    })
  }

  render() {

    const articleTitle = this.state.articleTitle
    const photoStory = this.state.photo

    return(
      <section className="about-us">
        <div className="wrap">

          <ArticleTitle
            isArticle={articleTitle.isArticle}
            title={articleTitle.title}
            subtitle={articleTitle.subtitle} />

          <Paragraph
            title={this.state.paragraph} />

          <div className="photo-story">
            {photoStory.map((photo) => {
              return (
                <PhotoStory
                  img={photo.photography}
                  icons={photo.icons}
                  title={photo.title}
                  key={photo.id}
                  changeParagraph={this.changeParagraph.bind(this, photo.title)}
                />
              )
            })}
          </div>
        </div>
      </section>
    )
  }
}

export default AboutUs