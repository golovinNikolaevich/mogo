import React from 'react'
import '../../Component/PhotoStory/PhotoStory.scss'
import './PhotoOurTeam.scss'

export default (props) => {
  const style = {
    background: `url(${props.img})`
  }

  return(
    <>
      <div 
        className="photo-story__item photo-team"
        title={props.title}
        style={style}
      >

        <div
          className="photo-story__item-hover"
          style={style}
        >
          
          <div className="photo-story__item-details">
            <p>{props.title}</p>
          </div>
        </div>
      </div>
    </>
  )
}