import React from 'react'
import './Facts.scss'
import Fact from '../../Component/Fact/Fact'

class Facts extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      facts: [
        { count: 42, what: 'What Design Projects', id: 1 },
        { count: 123, what: 'Happy Client', id: 2 },
        { count: 15, what: 'Award Winner', id: 3 },
        { count: 99, what: 'Cup of Coffe', id: 4 },
        { count: 24, what: 'Members', id: 5 }
      ]
    }
  }

  render() {

    const facts = this.state.facts

    return (
      <section className="facts">
        <div className="wrap">
          <div className="facts__contain">
            {facts.map((fact) => {
              return (
                <Fact
                count={fact.count}
                what={fact.what}
                key={fact.id} />
              )
            })}
          </div>
        </div>
      </section>
    )
  }
}

export default Facts