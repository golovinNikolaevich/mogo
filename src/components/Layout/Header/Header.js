import React from 'react'
import './Header.scss'
import Navigation from '../../Component/Navigation/Navigation'
import ArticleTitle from '../../Component/ArticleTitle/ArticleTitle'
import Button from '../../Component/Button/Button'

class Header extends React.Component {

  constructor(props) {
    super(props)
    
    this.state = {
      logoTitle: 'MoGo',
      articleTitle: {
        title: 'Creative Template',
        subtitle: 'Welcome to mogo',
        isArticle: false
      }
    }
  }

  // Добавление событий
  changeTitleHandler = () => {
    const oldLogoTitle = this.state.logoTitle
    const newLogoTitle = oldLogoTitle + ' (changed)'

    // Когда вызываеться метод сетСтате рендер компонента перезагружаеться
    this.setState({
      logoTitle: newLogoTitle
    })
  }

  render() {
    const articleTitle = this.state.articleTitle

    return(
      <header className="header">

        <div className="wrap">

          <div className="header__title-nav">
            <h1 className="header__title">{this.state.logoTitle}</h1>
            <div className="header__nav">
              <Navigation />
            </div>
          </div>

          <div className="header__text-box">
              <ArticleTitle
                isArticle={articleTitle.isArticle}
                title={articleTitle.title}
                subtitle={articleTitle.subtitle}
              />
              <Button
                btnName={'Learn more'} />

              {/* <button className="btn" onClick={this.changeTitleHandler}>Onclick</button> */}

          </div>

          <div className="header__slider">
            <a href="http://" className="header__slider-item">
              <p><span>01</span>Intro</p>
            </a>
            <a href="http://" className="header__slider-item">
              <p><span>02</span>Work</p>
            </a>
            <a href="http://" className="header__slider-item">
              <p><span>03</span>About</p>
            </a>
            <a href="http://" className="header__slider-item">
              <p><span>04</span>Contacts</p>
            </a>
          </div>

        </div>

      </header>
    )
  }
}

export default Header