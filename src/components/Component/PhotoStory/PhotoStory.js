import React from 'react'
import './PhotoStory.scss'

export default (props) => {
  const style = {
    background: `url(${props.img})`
  }

  return(
    <>
      <div 
        className="photo-story__item"
        onClick={props.changeParagraph}
        title={props.title}
        style={style}>

        <div
          className="photo-story__item-hover"
          style={style}>
          
          <div className="photo-story__item-details">
            <img src={props.icons} alt=""/>
            <p>{props.title}</p>
          </div>
        </div>
      </div>
    </>
  )
}