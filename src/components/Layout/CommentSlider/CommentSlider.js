import React from 'react'
import './CommentSlider.scss'
// Images
import speech from '../../../img/speech.png'

class CommentSlider extends React.Component {
  

  render() {
    return (
      <section className="comment-slider">
        <div className="comment-slider__wrap">
          <div className="comment-slider__contain">

            <div className="comment-slider__item">
              <img className="comment-slider__img" src={speech} alt="speech"></img>
              <div className="comment-slider__text-block">
                <div className="comment-slider__text">
                  “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.”
                </div>
                <div className="comment-slider__author">John Doe</div>
              </div>
            </div>

            <div className="comment-slider__item">
              <img className="comment-slider__img" src={speech} alt="speech"></img>
              <div className="comment-slider__text-block">
                <div className="comment-slider__text">
                  exercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitationexercitation
                </div>
                <div className="comment-slider__author">John Doe</div>
              </div>
            </div>

            <div className="comment-slider__item">
              <img className="comment-slider__img" src={speech} alt="speech"></img>
              <div className="comment-slider__text-block">
                <div className="comment-slider__text">
                  sitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsitsit
                </div>
                <div className="comment-slider__author">John Doe</div>
              </div>
            </div>

          </div>

          <div className="comment-slider__arrow-left">1</div>
          <div className="comment-slider__arrow-right" onClick={this.sliderRightHandler}>2</div>

        </div>
      </section>
      
    )
  }
}

export default CommentSlider

