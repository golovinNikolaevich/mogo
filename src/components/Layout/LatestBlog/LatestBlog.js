import React from 'react'
import ArticleTitle from '../../Component/ArticleTitle/ArticleTitle'

class LatestBlog extends React.Component {

  constructor() {
    super();
    this.title = 'Our stories';
    this.subtitle = 'Latest blog';
    this.isArticle = true;
  }

  render() {
    return(
      <section className="blog">
        <div className="wrap">
          
          <ArticleTitle
            isArticle={this.isArticle}
            title={this.title}
            subtitle={this.subtitle}
          />

        </div>
      </section>
    )
  }
}

export default LatestBlog